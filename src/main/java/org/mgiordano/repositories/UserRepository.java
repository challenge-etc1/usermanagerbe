package org.mgiordano.repositories;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

import org.mgiordano.entities.User;

import jakarta.enterprise.context.ApplicationScoped;


@ApplicationScoped
public class UserRepository implements PanacheRepository<User> {
    public User findByUsername(String username) {
        return find("username", username).firstResult();
    }
}
