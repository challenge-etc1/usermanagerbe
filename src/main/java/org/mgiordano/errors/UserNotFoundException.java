package org.mgiordano.errors;

import org.mgiordano.errors.constants.Errors;

public class UserNotFoundException extends RuntimeException {
    private final String code;

    public UserNotFoundException() {
        super(Errors.USER_NOT_FOUND.getMessage());
        this.code = Errors.USER_NOT_FOUND.getCode();
    }

    public String getCode() {
        return code;
    }
}
