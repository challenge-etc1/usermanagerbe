package org.mgiordano.errors.constants;

public enum Errors {
    USER_NOT_FOUND("E001", "User not found"),
    USER_ALREADY_EXISTS("E002", "User already exists");

    private final String code;
    private final String message;

    Errors(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}