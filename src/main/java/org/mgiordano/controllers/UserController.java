package org.mgiordano.controllers;

import jakarta.inject.Inject;
import org.mgiordano.controllers.requests.LoginRequest;
import org.mgiordano.controllers.responses.LoginResponse;
import org.mgiordano.entities.User;
import org.mgiordano.services.SecurityService;
import org.mgiordano.services.UserService;


import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.List;


@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserController {

    @Inject
    UserService userService;

    @Inject
    SecurityService securityService;

    @POST
    @Path("/register")
    public Response register(User user) {
        userService.register(user);
        user.setPassword(null);
        return Response.status(Response.Status.CREATED).entity(user).build();
    }

    @POST
    @Path("/login")
    public Response login(LoginRequest loginRequest) {
        String token = userService.login(loginRequest.getUsername(), loginRequest.getPassword());
        return Response.ok().entity(new LoginResponse(token)).build();
    }

    @GET
    @Path("/validateToken")
    public Response validateToken(@HeaderParam("Authorization") String authHeader) {
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            String token = authHeader.substring(7);
            if(securityService.validateToken(token)){
                return Response.ok().build();
            }
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @GET
    public Response getAllUsers() {
        List<User> users = userService.findAllUsers();
        return Response.ok(users).build();
    }

    @PUT
    @Path("/{userId}/password")
    public Response updatePassword(@PathParam("userId") Long userId, @QueryParam("currentPassword") String currentPassword, @QueryParam("newPassword") String newPassword) {
        if (userService.validateAndUpdatePassword(userId, currentPassword, newPassword)) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @PUT
    @Path("/{userId}/phone")
    public Response updatePhone(@PathParam("userId") Long userId, @QueryParam("phone") String phone) {
        userService.updatePhone(userId, phone);
        return Response.ok().build();
    }

    @PUT
    @Path("/{userId}/age")
    public Response updateAge(@PathParam("userId") Long userId, @QueryParam("age") int age) {
        userService.updateAge(userId, age);
        return Response.ok().build();
    }

    @PUT
    @Path("/{userId}/gender")
    public Response updateGender(@PathParam("userId") Long userId, @QueryParam("gender") String gender) {
        userService.updateGender(userId, gender);
        return Response.ok().build();
    }

}
