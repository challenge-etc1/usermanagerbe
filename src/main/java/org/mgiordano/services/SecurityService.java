package org.mgiordano.services;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import jakarta.enterprise.context.ApplicationScoped;
import org.mgiordano.entities.User;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class SecurityService {
    private static final String ALGORITHM = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding";
    private static final String SECRET_KEY_STRING = "14c309622fbf241dcae63a1d88e81612e59e18ab6916c71dbc1656f9b0e7520083f5c5ccadf885f16cfc08b39418ddf8da9be0c682aa34ed7efbade90968109e1773e8377dec6162c24d2ef0467727fe62c90a0ea0f88f2371342e122a99bb715be444b010d52c0b473646592499a089f76e6c4cb120faf303acceef7695a7c559c1c9ef2b118fd95e183721449bf811c895832e850ead9c8b7c7b5e4eda62333eaeadd9099d073e184fdacaba27d6fbfcee2dac7182d76f601432ef517fd9e3691e9918cb38969399844ffa19d50b08a292ab049220cb8e9ebc9d7b26313d362a2523f466b0b5e666761af6a8f3cc1cec5d8a4734467a37b49d1ccca197874f711b952ca7c27ac91cd68a0f3acf9a69c0c7378c3ee603c9c892e7a3809f9ac6204a6d9da636cdbdc1bc060544c0a0125ac1f91dc8cb2fc07c1209aef54479d2";
    private static final SecretKey SECRET_KEY = Keys.hmacShaKeyFor(SECRET_KEY_STRING.getBytes(StandardCharsets.UTF_8));
    private KeyPair keyPair;

    public SecurityService() {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(2048);
            this.keyPair = keyGen.generateKeyPair();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String generateToken(User user) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", user.getUsername());
        claims.put("role", "user");
        String csrfToken = generateCsrfToken();
        claims.put("csrfToken", csrfToken);
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(user.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 15)) // 15 minutes
                .signWith(SECRET_KEY, SignatureAlgorithm.HS256)
                .compact();
    }

    private String generateCsrfToken() {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[32];
        random.nextBytes(bytes);
        return Base64.getUrlEncoder().withoutPadding().encodeToString(bytes);
    }

    public boolean validateToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parserBuilder()
                    .setSigningKey(SECRET_KEY)
                    .build()
                    .parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String encryptPassword(String password) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());
            return Base64.getEncoder().encodeToString(cipher.doFinal(password.getBytes()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String decryptPassword(String encryptedPassword) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());
            return new String(cipher.doFinal(Base64.getDecoder().decode(encryptedPassword)));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public boolean validatePassword(String inputPassword, String storedEncryptedPassword) {
        String decryptedPassword = decryptPassword(storedEncryptedPassword);
        return inputPassword.equals(decryptedPassword);
    }
}