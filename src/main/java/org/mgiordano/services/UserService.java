package org.mgiordano.services;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mgiordano.entities.User;
import org.mgiordano.errors.UserAlreadyExistsException;
import org.mgiordano.errors.UserNotFoundException;
import org.mgiordano.repositories.UserRepository;

import java.util.List;

@ApplicationScoped
public class UserService {

    private static final Logger logger = LogManager.getLogger(UserService.class);

    @Inject
    UserRepository userRepository;

    @Inject
    SecurityService securityService;

    @Transactional
    public User register(User user) {
        logger.info("Registering user: {}", user.getUsername());
        if (userRepository.findByUsername(user.getUsername()) != null) {
            logger.warn("User already exists: {}", user.getUsername());
            throw new UserAlreadyExistsException();
        }
        user.setPassword(securityService.encryptPassword(user.getPassword()));
        userRepository.persist(user);
        logger.info("User registered successfully: {}", user.getUsername());
        return user;
    }

    @Transactional
    public String login(String username, String password) {
        logger.info("Attempting to login user: {}", username);
        User user = userRepository.findByUsername(username);
        if (user == null || !securityService.validatePassword(password, user.getPassword())) {
            logger.error("Login failed for user: {}", username);
            throw new UserNotFoundException();
        }
        String token = securityService.generateToken(user);
        logger.info("User logged in successfully: {}", username);
        return token;
    }

    @Transactional
    public List<User> findAllUsers() {
        logger.info("Retrieving all users");
        List<User> users = userRepository.findAll().list();
        if (users.isEmpty()) {
            logger.info("No users found");
        } else {
            logger.info("Found {} users", users.size());
        }
        return users;
    }

    @Transactional
    public boolean validateAndUpdatePassword(Long userId, String currentPassword, String newPassword) {
        User user = userRepository.findById(userId);
        if (user != null && securityService.validatePassword(currentPassword, user.getPassword())) {
            String newEncryptedPassword = securityService.encryptPassword(newPassword);
            user.setPassword(newEncryptedPassword);
            userRepository.persist(user);
            return true;
        }
        return false;
    }

    @Transactional
    public void updatePhone(Long userId, String phone) {
        User user = userRepository.findById(userId);
        if (user != null) {
            user.setPhoneNumber(phone);
            userRepository.persist(user);
        }
    }

    @Transactional
    public void updateAge(Long userId, Integer age) {
        User user = userRepository.findById(userId);
        if (user != null) {
            user.setAge(age);
            userRepository.persist(user);
        }
    }

    @Transactional
    public void updateGender(Long userId, String gender) {
        User user = userRepository.findById(userId);
        if (user != null) {
            user.setGender(gender);
            userRepository.persist(user);
        }
    }

}
