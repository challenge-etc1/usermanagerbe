package org.mgiordano.errors;

import org.mgiordano.errors.constants.Errors;

public class UserAlreadyExistsException extends RuntimeException {
    private final String code;

    public UserAlreadyExistsException() {
        super(Errors.USER_ALREADY_EXISTS.getMessage());
        this.code = Errors.USER_ALREADY_EXISTS.getCode();
    }

    public String getCode() {
        return code;
    }
}
