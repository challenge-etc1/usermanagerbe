package org.mgiordano.controllers.errorHandlers;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.mgiordano.errors.UserAlreadyExistsException;
import org.mgiordano.errors.UserNotFoundException;
import org.mgiordano.controllers.responses.ErrorResponse;

@Provider
public class UserErrorHandler implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable exception) {
        if (exception instanceof UserAlreadyExistsException e) {
            return handleUserAlreadyExistsException(e);
        } else if (exception instanceof UserNotFoundException e) {
            return handleUserNotFoundException(e);
        } else {
            System.out.println(exception.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorResponse("E999", "Internal server error"))
                    .build();
        }
    }

    private Response handleUserAlreadyExistsException(UserAlreadyExistsException exception) {
        return Response.status(Response.Status.CONFLICT)
                .entity(new ErrorResponse(exception.getCode(), exception.getMessage()))
                .build();
    }

    private Response handleUserNotFoundException(UserNotFoundException exception) {
        return Response.status(Response.Status.UNAUTHORIZED)
                .entity(new ErrorResponse(exception.getCode(), exception.getMessage()))
                .build();
    }
}
